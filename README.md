Incident Response Server -  Server used by Chrome Extension Implementation of 
                            the IR tool. Necessary since chrome extensions don't
                            have access to offline storage resources, to which
                            evidence and timings could be persisted

Files Contained:


## Collected Evidence
Contains the evidence files collected by the produced IR
Tool, during various types of test attacks.

## domMutations
Contains extrapolated mutations produced from running
compareEvidence.sh.

## compareEvidence.sh
Bash script that compares collected evidence against, a
clean version of it, and extrapolates DOM mutations,
irrespective of them being malicious or not. Incident
Responder would then analyse these mutations to deduce
whether they are malicious or not (as proposed in the 
forensic analysis technique produced)

## node_modules
npm packages

## timings
DevTools Portocol Monitoring tool; evidence collection
overheads

## visualizations
File containing visualizations of statistical data based
upon the timing overheads collected. These are produced
using the python script runEvaluation.py.

## server.js
RESTful server architecture to handle post and get 
requests made by ir tool chrome extension version.
