console.log('server is starting using express framework');

//declaration to use filesystem on machine running server
var fs = require('fs');
var path = require('path');

//decleration to use performance-now package to time functions
var now = require("performance-now")
var start = now()
var end = now()


//standard express setup
var express = require('express');
var app = express();

//standard body parser setup to handle post request
var bodyParser = require('body-parser');


//increasing size of payload to handle larger evidence files
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(bodyParser.json({limit: '50mb'}));


//setup the port for the server
var server = app.listen(3000);

//set post handler
app.post('/saveEvidence', saveEvidence);
app.post('/saveTimings', saveTimings);

//global array to hold timings of saveEvidence
var timings = [];

//holds sample count
var counter = 0;

//save evidence recieved
function saveEvidence(request,response){
	let t0 = now();
	let data = request.body;
	let fileName = data.name;
	let date = parseDate(data.evidence.TimeStamp);
	let evidence = JSON.stringify(data.evidence, null, 2);

	let directory = './collectedEvidence/'+date+'/';

	//check if today's data has already started being collected
	//if yes start saving in it else create it and start saving to it
	try{
		fs.statSync(directory);
		//writing evidence to file
		fs.writeFile(directory+fileName+'.json', evidence, () => console.log('Evidence Saved'));
	}catch(e){
		fs.mkdirSync(directory);
		//writing evidence to file
		fs.writeFile(directory+fileName+'.json', evidence, () => console.log('Evidence Saved'));
	}

	//send empty response to client so that it stops waiting for response
	//when this was missing, client was getting clogged up waiting for responses
	response.end();
	let t1 = now();
	let saveEvidenceTiming = t1-t0;
	timings.push({[counter] : {'saveEvidence': saveEvidenceTiming}});
	counter+=1;
}

//get timestamp object and parse today's date
function parseDate(timestamp){
	let date = timestamp.Date + '_' + timestamp.Month + '_' + timestamp.Year;
	return date;
}

//combines recieved timings to the ones captured on server
//and then saves them
function saveTimings(request,response){
	let data = request.body;
	let array_of_objects = Object.values(data);
	let mergedTimings = [];
	array_of_objects.forEach((itm,i)=>{
		mergedTimings.push(Object.assign(timings[i][i], itm))
	});

	//create file stream to be able to append timings to it
	//this approach was used since it is more efficient than using append.
	//append creates a handle each time and since we will be appending multiple times
	//would create a whole mess wrt. file system handling.
	//https://stackoverflow.com/questions/3459476/how-to-append-to-a-file-in-node/43370201#43370201
	var stream = fs.createWriteStream("./timings/log.csv", {flags:'a'});
	stream.write("DOMSize,captureEvidence,sendBackground,packEvidence,sendEvidence,saveEvidence,totalTime\n");

	//sorts values of each sample object and writes it to file
	mergedTimings.forEach(object =>{
		let parsedObjectData = Object.entries(object);
		let sortedObject = sortValues(parsedObjectData);
		let stringifiedSortedObject = sortedObject.toString();
		stream.write(stringifiedSortedObject+'\n');
	});

	stream.end();

	
	//send empty response to client so that it stops waiting for response
	//when this was missing, client was getting clogged up waiting for responses
	response.end();
}

//function that gets the object values in a form of 2d array
//where each array index contains another array - this time it contains event type followed by timing
function sortValues(array_single_object){
	let sorted_values = [];
	let total_time = 0;
	array_single_object.forEach( inner_array => {
		switch(inner_array[0]){
			case 'saveEvidence':
				sorted_values[5] = inner_array[1];
				total_time += inner_array[1];
				break;
			case 'captureEvidence':
				sorted_values[1] = inner_array[1];
				total_time += inner_array[1];
				break;
			case 'sendBackground':
				sorted_values[2] = inner_array[1];
				total_time += inner_array[1];
				break;
			case 'DOMSize':
				sorted_values[0] = inner_array[1];
				break;
			case 'packEvidence':
				sorted_values[3] = inner_array[1];
				total_time += inner_array[1];
				break;
			case 'sendEvidence':
				sorted_values[4] = inner_array[1];
				total_time += inner_array[1];
				break;
			default:
				console.log('Error In Sorting Object -- Unkown Field');
				break;
		}
		sorted_values[6] = total_time;
	});
	return sorted_values;
}